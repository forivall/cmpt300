/* 
 * inc source for increment program
 * Jordan Klassen, 2008
 * 
 * 
 * 
 *
 * 
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int inc(int n, int fildes) {
  int i;
  char buffer[20]; //20 is max length for 64 bit int
  
  //read file  
  lseek(fildes,0,SEEK_SET);
  ftruncate(fildes, sizeof(buffer));
  read(fildes,buffer,sizeof(buffer)+1);

  //convert to int
  i = atoi(buffer);
  printf("%d",i);
  //increment by n
  i += n;
  printf("->%d\n",i);
  
  //convert to string
  sprintf(buffer,"%-*d",sizeof(buffer),i);
  
  // find how long the number actually is
  // i is being reused, yay!
  for(i = 1;i<sizeof(buffer);++i)
  {
    if((buffer[i] < 48)||(buffer[i] > 57))
    {
      buffer[i] = '\n';
      ++i;
      break;
    }
  }
  
  //output to file
  lseek(fildes,0,SEEK_SET);
  ftruncate(fildes, i);
  return(write(fildes, buffer, i) != 1);
}  /* end inc */

