
#ifndef _INC_H_
#define _INC_H_

/* you may NOT change this interface */
/* return 0 if error free; nonzero if an error occurred */
int inc(int n, int fildes);

#endif /* _INC_H_ */

