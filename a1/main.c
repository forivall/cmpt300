/* 
 * Main for increment program
 * Jordan Klassen, 2008
 * 
 * 
 * 
 *
 * 
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
//stdlib needed for atoi
#include "inc.h"

main (int argc, char **argv) {
  int i;
  int n, counterfile;
  
  //bools to check switches
  int reset = 0, n_set = 0, f_set = 0;
  char* filename;
  n = 1;
  counterfile = 1;
  
  if(argc <= 1)
  {
    printf("Missing file name\n");
    printf("Usage: inc [-r] [-n] [file]\n");
    return -1;
  }

  for (i = 1; i < argc; ++i) 
  {
    
    //printf("Argument %2d = [%s]\n", i, argv[i]);
    if(argv[i][0] == '-')
    {
      if(argv[i][1] == 'r')
      {
        if(reset)
          printf("One -r switch is good enough. Sheesh.\n");
        reset = 1;
      }
      else if((argv[i][1] >= '0')&&(argv[i][1] <= '9'))
      {
        if(n_set)
        {
          printf("TWO numbers! Run away!!!\n");
          return -1;
        }
        n_set = 1;
        argv[i][0] = ' ';
        n = atoi(argv[i]);
      }
      else
      {
        printf("Unknown argument \"%s\"\n",argv[i]);
        printf("Usage: inc [-r] [-n] [file]\n");
        return -1;;
      }
    }//end of switches
    else if(!f_set)
    {
      filename = argv[i];
      f_set = 1;
    }
    else
    {
      printf("File already defined once\n");
      return -1;
    }
    
  }//end of arguments
  
  counterfile = open(filename, O_RDWR);
  if(counterfile < 0)
  {
    printf("File not found or is a folder.\n");
    return -1;
  }
  
  if (reset)
  {
    
    ftruncate(counterfile, 1);
    write(counterfile, "0\n",2);
    lseek(counterfile,0,SEEK_SET);
  }
  if(!reset | n_set)
  {
    //printf("inc returned %d.\n", 
    inc(n, counterfile);
    //);
  }

  close(counterfile);

} /* main */

