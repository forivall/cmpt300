
#ifndef decrypt_hpp
#define decrypt_hpp

//char* remchars(char*);

int cipher2plain(char);
char plain2cipher(int);

long long text2cryptnum(char*);
char* cryptnum2text(long long);

long long fastexp(long long);

#endif /* decrypt_hpp */
