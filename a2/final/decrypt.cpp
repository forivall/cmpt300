/* 
 * various functions for decrypt program
 * Jordan Klassen, Feb 2009
 * 
 */

#include <stdlib.h>
#include <string.h>
 
#include "decrypt.hpp"

// converts a ciphertext character to a plaintext number
int cipher2plain(char c)
{
	int n = -1;
	if((c >= 'a') && (c <= 'z')) {
		n = c-'a'+1;	
	}
	else {
		switch (c) {
			case ' ': n = 0; break;
			case '\n': n = 27; break;
			case '.': n = 28; break;
			case ',': n = 29; break;
			case '\'': n = 30; break;
			case '!': n = 31; break;
			case '?': n = 32; break;
			case '(': n = 33; break;
			case ')': n = 34; break;
			case '-': n = 35; break;
			case ':': n = 36; break;
			case '$': n = 37; break;
			case '/': n = 38; break;
			case '&': n = 39; break;
			case '\\': n = 40; break;
		}
	}
	
	return n;
}

// converts a plaintext number to the corresponding character
char plain2cipher(int n)
{
	char c='\0';
	if((n >= 1) && (n <= 26)) 
	{
		c = n-1+'a';	
	}
	else {
		switch (n) {
			case 0: c = ' '; break;
			case 27: c = '\n'; break;
			case 28: c = '.'; break;
			case 29: c = ','; break;
			case 30: c = '\''; break;
			case 31: c = '!'; break;
			case 32: c = '?'; break;
			case 33: c = '('; break;
			case 34: c = ')'; break;
			case 35: c = '-'; break;
			case 36: c = ':'; break;
			case 37: c = '$'; break;
			case 38: c = '/'; break;
			case 39: c = '&'; break;
			case 40: c = '\\'; break;
		}
	}
	return c;
}
 
// quicker than actually calculating, i guess
// gets the power of 41 to a maximum of 41^9
long long pow_41(int exp)
{
	long long n = 1;
	switch (exp) {
		case 1: n = 41; break;
		case 2: n = 1681; break;
		case 3: n = 68921; break;
		case 4: n = 2825761; break;
		case 5: n = 115856201; break;
		case 6: n = 4750104241LL; break;
		case 7: n = 194754273881LL; break;
		case 8: n = 7984925229121LL; break;
		case 9: n = 327381934393961LL; break;
	}
	return n;
}

// converts a string to the cryptography number
long long text2cryptnum(char *in)
{
	long long n = 0, ssize = strlen(in);
	for(int i = 0;i<ssize;++i)
	{
		n += cipher2plain(in[i]) * pow_41(ssize-i-1);
	}
	
	return n;
}

// converts a cryptography number to a string
char* cryptnum2text(long long n)
{
	int ssize = 6;
	char *out;
	out = (char*) malloc(7);
		
	for(int i = 0;i<ssize;++i)
	{
		out[i] = plain2cipher((int) (n/pow_41(ssize-i-1)));
		n = n%pow_41(ssize-i-1);
	}
	out[6] = '\0';
	return out;
}
 
// computes the exponentization quickly for the encryption
long long fastexp(long long c) // c = base
{
	unsigned long long d=1921821779LL, n=4294434817LL, y=1; //exp, mod, result
	
	// found this algorithm in wikipedia
	// http://en.wikipedia.org/wiki/Addition-chain_exponentiation
	// and added modulo operators
	int iter=0;
		
	while(d > 0){
		if(d % 2 == 1){
			y = (c*y) % n;
		}
		
		d = d / 2;
		c = (c * c) % n;
		++iter;
	}
	
	return (long long)y;
}
