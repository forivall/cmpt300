/* 
 * various functions for decrypt program
 * Jordan Klassen, Feb 2009
 * 
 */
#ifndef decrypt_hpp
#define decrypt_hpp

long long text2cryptnum(char*);
char* cryptnum2text(long long);

long long fastexp(long long);

#endif /* decrypt_hpp */
