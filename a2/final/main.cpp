/* 
 * main method for decrypt program
 * Jordan Klassen, Feb 2009
 * 
 * help with pthreads mainly from wikipedia and 
 * http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html
 * 
 */

// C includes
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>

// Local includes
#include "decrypt.hpp"
#include "queue64LL.hpp"
#include "queue64char.hpp"
 
//using namespace std;

// mutexes for queues
pthread_mutex_t char_mutex = PTHREAD_MUTEX_INITIALIZER,
				c_num_mutex = PTHREAD_MUTEX_INITIALIZER,
				p_num_mutex = PTHREAD_MUTEX_INITIALIZER;
			
// queue for the characters from stdin for step 1 to 2
queue64char char_q;
bool step1_done;

// queue for cipher numbers from step 2 to 3
queue64LL c_num_q;
bool step2_done;

// queue for plaintext numbers from step 3 to 4
queue64LL p_num_q;
bool step3_done;

// done markers don't really need mutexes since they are only written by one thread


void *step1(void *)
{	
	char c;
	int i=0;
	while(feof(stdin) == 0)
	{
		pthread_mutex_lock(&char_mutex);
		
		//populate queue
		while(!char_q.full())
		{
			c = getchar();
			if(feof(stdin) == 0)
			{
				if(!(i%8 == 7))
				{
					char_q.push(c);
				}
			}
			else break;
			++i;
		}
		
		pthread_mutex_unlock(&char_mutex);
		sched_yield();
	}
	
	step1_done = true;
	
}

void *step2(void *)
{
	// idea for locking scheme from 
	// http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html#PITFALLS
	do
	{
		pthread_mutex_lock(&char_mutex);
		while ( pthread_mutex_trylock(&c_num_mutex) )  // Test if already locked   
		{
		   pthread_mutex_unlock(&char_mutex);  // Free resource to avoid deadlock 
		   sched_yield();
		   pthread_mutex_lock(&char_mutex);
		}
		
		// get strings from queue and convert them into a number
		while(char_q.get_size() >= 6)
		{
			if(c_num_q.full()) //give up
			{
				break;
			}
			else
			{
				char temp[7];
				for(int i=0;i<6;++i)
				{
					temp[i] = char_q.pop();
				}
				temp[6] = '\0'; // I HATE c strings. oh well.
				
				c_num_q.push(text2cryptnum(temp));
			}
		}
		pthread_mutex_unlock(&c_num_mutex);
		pthread_mutex_unlock(&char_mutex);
		sched_yield();
	}
	while(!step1_done);
	
	// extra time in case there's still more in the queue
	while ( pthread_mutex_trylock(&c_num_mutex) )  // Test if already locked   
	{
	   pthread_mutex_unlock(&char_mutex);  // Free resource to avoid deadlock 
	   sched_yield();
	   pthread_mutex_lock(&char_mutex);
	}
	
	while(char_q.get_size() >= 6)
	{

		if(c_num_q.full())//give up
		{
			break;
		}
		else
		{
			char temp[7];
			for(int i=0;i<6;++i)
			{
				temp[i] = char_q.pop();
			}
			temp[6] = '\0'; // I HATE c strings. oh well.
			//mutex stuff
			c_num_q.push(text2cryptnum(temp));
		}
	}
	pthread_mutex_unlock(&c_num_mutex);
	pthread_mutex_unlock(&char_mutex);
	//end extra
	
	step2_done = true;
}

void *step3(void *)
{
	do
	{
		pthread_mutex_lock(&p_num_mutex);
		while ( pthread_mutex_trylock(&c_num_mutex) ) // test lock
		{
		   pthread_mutex_unlock(&p_num_mutex);  // avoid deadlock
		   sched_yield();
		   pthread_mutex_lock(&p_num_mutex);
		}
		
		// convert cipher number to plaintext number
		while(!c_num_q.empty())
		{
			if(p_num_q.full())//give up
			{
				break;
			}
			else
			{
				p_num_q.push(fastexp(c_num_q.pop()));
			}
		}
		
		pthread_mutex_unlock(&c_num_mutex);
		pthread_mutex_unlock(&p_num_mutex);
		sched_yield();
	}
	while(!step2_done);
	
	// one extra time, in case more written to queue
	pthread_mutex_lock(&p_num_mutex);
	while ( pthread_mutex_trylock(&c_num_mutex) ) // test lock
	{
	   pthread_mutex_unlock(&p_num_mutex);  // avoid deadlock 
	   sched_yield();
	   pthread_mutex_lock(&p_num_mutex);
	}
	
	// convert cipher number to plaintext number
	while(!c_num_q.empty())
	{
		if(p_num_q.full())// give up
		{
			break;
		}
		else
		{
			p_num_q.push(fastexp(c_num_q.pop()));
		}
	}
	pthread_mutex_unlock(&c_num_mutex);
	pthread_mutex_unlock(&p_num_mutex);
	// end extra time
	
	step3_done = true;
}

void *step4(void *)
{
	do
	{
		pthread_mutex_lock(&p_num_mutex);
		
		//output!
		while(!p_num_q.empty())
		{
			
			char *temp = cryptnum2text(p_num_q.pop());
			int len = strlen(temp);
			for(int i=0;temp[i] > len;++i)
			{
				putchar(temp[i]);
			}
		}
		/*printf("\nQueue 1 size: %d\nQueue 2 size: %d\nQueue 3 size: %d\n", 
				char_q.get_size(), c_num_q.get_size(), p_num_q.get_size());*/
			
		pthread_mutex_unlock(&p_num_mutex);
		sched_yield();
	}
	while(!step3_done);
	
	//one extra time in case there's more in the queue
	pthread_mutex_lock(&p_num_mutex);
	
	//output!
	while(!p_num_q.empty())
	{
		char *temp = cryptnum2text(p_num_q.pop());
		int len = strlen(temp);
		for(int i=0;temp[i] > len;++i)
		{
			putchar(temp[i]);
		}
	}
	pthread_mutex_unlock(&p_num_mutex);
	//end extra
}
 
int main(void)
{
	// create vars
	pthread_t step1_th, step2_th, step3_th, step4_th;
	step1_done = false;
	step2_done = false;
	step3_done = false;
	
	// init threads
	
	if (pthread_create(&step1_th, NULL, step1, NULL) != 0) {
		printf("fail!");
		return EXIT_FAILURE;
	}
	
	if (pthread_create(&step2_th, NULL, step2, NULL) != 0) {
		printf("fail!");
		return EXIT_FAILURE;
	}
	
	if (pthread_create(&step3_th, NULL, step3, NULL) != 0) {
		printf("fail!");
		return EXIT_FAILURE;
	}
	
	if (pthread_create(&step4_th, NULL, step4, NULL) != 0) {
		printf("fail!");
		return EXIT_FAILURE;
	}
	
	// end threads
	
	if (pthread_join(step1_th, NULL) != 0)
    {
		printf("fail!");
        return EXIT_FAILURE;
    }
	if (pthread_join(step2_th, NULL) != 0)
    {
		printf("fail!");
        return EXIT_FAILURE;
    }
	if (pthread_join(step3_th, NULL) != 0)
    {
		printf("fail!");
        return EXIT_FAILURE;
    }
	if (pthread_join(step4_th, NULL) != 0)
    {
		printf("fail!");
        return EXIT_FAILURE;
    }
	
	// end program
	
	return EXIT_SUCCESS;
}
