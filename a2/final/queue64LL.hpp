// 64 byte fixed length long long int queue class
// Jordan Klassen, Feb 2009

#ifndef queue64LL_hpp
#define queue64LL_hpp

class queue64LL
{
public:
	queue64LL();
	~queue64LL();
	bool empty();
	bool full();
	int get_size();
	long long pop();
	void push(long long);
	/* void print(); */
	
private:
	long long data[8];
	int back;
	int front;
	int size;
};


#endif /* queue64_hpp */
