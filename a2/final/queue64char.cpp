// 64 byte fixed length char queue class
// Jordan Klassen, Feb 2009

// commented code is for debugging
// methods are self explanatory

//#include <iostream>

#include "queue64char.hpp"

queue64char::queue64char():
	back(0),
	front(0),
	size(0)
{
}

queue64char::~queue64char()
{
}

bool queue64char::empty(){
	return (size <= 0);
}
bool queue64char::full(){
	return (size >= 64);
}

int queue64char::get_size(){
	return size;
}

char queue64char::pop(){
	char out = data[front];
	front = (front+1)%64;
	--size;
	return out;
	
}
void queue64char::push(char in){
	data[back] = in;
	back = (back+1)%64;
	++size;
}

/* void queue64char::print()
{
	int j = front;
	for(int i = 0;i<size;++i)
	{
		std::cout << data[j];
		j = (j+1)%64;
	}
	std::cout << "\n";
} */
