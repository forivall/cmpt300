/* 
 * functions for decrypt program
 * Jordan Klassen, 2008
 * 
 * 
 * 
 *
 * 
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>

 
#include <iostream>
#include <queue>
#include <string>
 
#include "decrypt.hpp"
#include "queue64LL.hpp"
#include "queue64char.hpp"

#define yield() pthread_yield()

using namespace std;
 
pthread_mutex_t char_mutex = PTHREAD_MUTEX_INITIALIZER,
				c_num_mutex = PTHREAD_MUTEX_INITIALIZER,
				p_num_mutex = PTHREAD_MUTEX_INITIALIZER;
			
// queue for the characters from stdin for step 1 to 2
queue64char char_q;
bool step1_done;

// queue for cipher numbers from step 2 to 3
queue64LL c_num_q;
bool step2_done;

// queue for plaintext numbers from step 3 to 4
queue64LL p_num_q;
bool step3_done;

// done markers don't need mutexes since they are only written by one thread


void *step1(void *)
{	
	//populate queue
	//cout << "step1start" << endl;
	char c;
	int i=0;
	while(feof(stdin) == 0)
	{
		pthread_mutex_lock(&char_mutex);
		while(!char_q.full())
		{
			c = getchar();
			if(feof(stdin) == 0)
			{
				if(!(i%8 == 7))
				{
					char_q.push(c);
				}
			}
			else break;
			++i;
		}
		pthread_mutex_unlock(&char_mutex);
		yield();
	}
	//cout << "step1end" << endl;
	step1_done = true;
	
}

void *step2(void *)
{
	//cout << "step2start" << endl;
	//idea from http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html#PITFALLS
	do
	{
		pthread_mutex_lock(&char_mutex);
		
		while ( pthread_mutex_trylock(&c_num_mutex) )  // Test if already locked   
		{
		   pthread_mutex_unlock(&char_mutex);  // Free resource to avoid deadlock 
		   yield();
		   pthread_mutex_lock(&char_mutex);
		}
		
		while(char_q.get_size() >= 6)
		{
			//printf("%d\n",char_q.get_size());
			//char_q.print();
			if(c_num_q.full())
			{//give up
				break;
			}
			else
			{
				char temp[7];
				for(int i=0;i<6;++i)
				{
					temp[i] = char_q.pop();
				}
				temp[6] = '\0'; // I HATE c strings. oh well.
				//mutex stuff
				c_num_q.push(text2cryptnum(temp));
			}
		}
		pthread_mutex_unlock(&c_num_mutex);
		pthread_mutex_unlock(&char_mutex);
		yield();
	}
	while(!step1_done);
	
	// extra time to fix bug
	while ( pthread_mutex_trylock(&c_num_mutex) )  // Test if already locked   
	{
	   pthread_mutex_unlock(&char_mutex);  // Free resource to avoid deadlock 
	   yield();
	   pthread_mutex_lock(&char_mutex);
	}
	
	while(char_q.get_size() >= 6)
	{
		//printf("%d\n",char_q.get_size());
		//char_q.print();
		if(c_num_q.full())
		{//give up
			break;
		}
		else
		{
			char temp[7];
			for(int i=0;i<6;++i)
			{
				temp[i] = char_q.pop();
			}
			temp[6] = '\0'; // I HATE c strings. oh well.
			//mutex stuff
			c_num_q.push(text2cryptnum(temp));
		}
	}
	pthread_mutex_unlock(&c_num_mutex);
	pthread_mutex_unlock(&char_mutex);
	//end extra
	
	//cout << "step2end" << endl;
	step2_done = true;
}

void *step3(void *)
{
	//cout << "step3start" << endl;
	do
	{
		pthread_mutex_lock(&p_num_mutex);
		while ( pthread_mutex_trylock(&c_num_mutex) )  /* Test if already locked   */
		{
		   pthread_mutex_unlock(&p_num_mutex);  /* Free resource to avoid deadlock */
		   yield();
		   pthread_mutex_lock(&p_num_mutex);
		}
		
		while(!c_num_q.empty())
		{
			if(p_num_q.full())
			{//give up
				break;
			}
			else
			{
				//mutex stuff
			p_num_q.push(fastexp(c_num_q.pop()));
			}
		}
		pthread_mutex_unlock(&c_num_mutex);
		pthread_mutex_unlock(&p_num_mutex);
		yield();
	}
	while(!step2_done);
	
	//one extra time, cuz it needs it
	pthread_mutex_lock(&p_num_mutex);
	while ( pthread_mutex_trylock(&c_num_mutex) )  /* Test if already locked   */
	{
	   pthread_mutex_unlock(&p_num_mutex);  /* Free resource to avoid deadlock */
	   yield();
	   pthread_mutex_lock(&p_num_mutex);
	}
	
	while(!c_num_q.empty())
	{
		if(p_num_q.full())
		{//give up
			break;
		}
		else
		{
			//mutex stuff
		p_num_q.push(fastexp(c_num_q.pop()));
		}
	}
	pthread_mutex_unlock(&c_num_mutex);
	pthread_mutex_unlock(&p_num_mutex);
	//end extra time
	
	//cout << "step3end" << endl;
	step3_done = true;
}

void *step4(void *)
{
	//cout << "step4start" << endl;
	do
	{
		pthread_mutex_lock(&p_num_mutex);
		while(!p_num_q.empty())
		{
			//output!
			char *temp = cryptnum2text(p_num_q.pop());
			int len = strlen(temp);
			for(int i=0;temp[i] > len;++i)
			{
				putchar(temp[i]);
			}
		}
		pthread_mutex_unlock(&p_num_mutex);
		yield();
	}
	while(!step3_done);
	
	//one extra time, cuz it needs it
	pthread_mutex_lock(&p_num_mutex);
	while(!p_num_q.empty())
	{
		//output!
		char *temp = cryptnum2text(p_num_q.pop());
		int len = strlen(temp);
		for(int i=0;temp[i] > len;++i)
		{
			putchar(temp[i]);
		}
	}
	pthread_mutex_unlock(&p_num_mutex);
	//end extra
	
	cout << endl;
	//cout << "step4end" << endl;
}
 
int main(void)
{
	pthread_t step1_th, step2_th, step3_th, step4_th;
	step1_done = false;
	step2_done = false;
	step3_done = false;
	
	//cout << endl << "step1_create" << endl;
	if (pthread_create(&step1_th, NULL, step1, NULL) != 0) {
		cout<<"fail!";
		return EXIT_FAILURE;
	}
	
	//cout << endl << "step2_create" << endl;
	if (pthread_create(&step2_th, NULL, step2, NULL) != 0) {
		cout<<"fail!";
		return EXIT_FAILURE;
	}
	
	//cout << endl << "step3_create" << endl;
	if (pthread_create(&step3_th, NULL, step3, NULL) != 0) {
		cout<<"fail!";
		return EXIT_FAILURE;
	}
	
	//cout << endl << "step4_create" << endl;
	if (pthread_create(&step4_th, NULL, step4, NULL) != 0) {
		cout<<"fail!";
		return EXIT_FAILURE;
	}
	
	if (pthread_join(step1_th, NULL) != 0)
    {
		cout<<"fail!";
        return EXIT_FAILURE;
    }
	if (pthread_join(step2_th, NULL) != 0)
    {
		cout<<"fail!";
        return EXIT_FAILURE;
    }
	if (pthread_join(step3_th, NULL) != 0)
    {
		cout<<"fail!";
        return EXIT_FAILURE;
    }
	if (pthread_join(step4_th, NULL) != 0)
    {
		cout<<"fail!";
        return EXIT_FAILURE;
    }


	/*
	cout << endl << "step1" << endl;
	step1();
	char_q.print();

	cout << endl << "step2" << endl;
	step2();
	c_num_q.print();
	
	cout << endl << "step3" << endl;
	step3();
	p_num_q.print();
	
	cout << endl << "step4" << endl;
	step4();
	p_num_q.print();
	*/
	
	return EXIT_SUCCESS;
}
