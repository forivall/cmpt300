#include <iostream>
#include "queue64LL.hpp"

queue64LL::queue64LL():
	back(0),
	front(0),
	size(0)
{
}

queue64LL::~queue64LL()
{
}

bool queue64LL::empty(){
	return (size <= 0);
}
bool queue64LL::full(){
	return (size >= 7);
}
long long queue64LL::pop(){
	int ind = front;
	front = (front+1)%8;
	--size;
	return data[ind];
	
}
void queue64LL::push(long long in){
	data[back] = in;
	back = (back+1)%8;
	++size;
}

void queue64LL::print()
{
	int j = front;
	for(int i = 0;i<size;++i)
	{
		long long out = data[j];
		std::cout << out << ",";
		j = (j+1)%10;
	}
}
