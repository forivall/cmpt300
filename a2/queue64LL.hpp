#ifndef queue64LL_hpp
#define queue64LL_hpp

class queue64LL
{
public:
	queue64LL();
	~queue64LL();
	bool empty();
	bool full();
	long long pop();
	void push(long long);
	void print();
	
private:
	long long data[8];
	int back;
	int front;
	int size;
};


#endif /* queue64_hpp */
