#ifndef queue64char_hpp
#define queue64char_hpp

class queue64char
{
public:
	queue64char();
	~queue64char();
	bool empty();
	bool full();
	int get_size();
	char pop();
	void push(char);
	void print();
	
private:
	char data[64];
	int back;
	int front;
	int size;
};


#endif /* queue64_hpp */
