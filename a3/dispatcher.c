
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>
#include "global.h"

#define MALE_EXEC "./male"
#define FEMALE_EXEC "./female"


//=========================
// do not modify, much
//=========================
main (int argc, char *argv[], char *envp[]) {
  int i;
  int f_ret;

  if (DEBUG) printf("Dispatcher started...\n");

  for (i = 1; i <= 20; i++) {
    sleep(1);
    if (DEBUG) printf("Dispatcher tick %d...\n", i);
    if ((i % 6) == 0) {
      f_ret = fork();
      if (f_ret < 0) { perror("fork"); }
      else if (f_ret == 0) { execve(MALE_EXEC, argv, envp); }
    }
    if ((i % 7) == 0) {
      f_ret = fork();
      if (f_ret < 0) { perror("fork"); }
      else if (f_ret == 0) { execve(FEMALE_EXEC, argv, envp); }
    }
  }

  if (DEBUG) printf("Dispatcher terminating...\n");
} // dispatcher


