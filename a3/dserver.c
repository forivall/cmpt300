
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>

#include <string.h>

#include "global.h"
#include "queue_msg.hpp"

main () {
	int q_id;
	int ret;
	int i, j;
	ssize_t actual_size;
	msg_60 msg_in, msg_out;

	queue_msg msgs;
	char q_has_males = 0;

	// establish the queue
	if (DEBUG) printf("Dating Server creating message queue...\n");
	q_id = msgget(DSERV_QKEY, IPC_CREAT | 0666);
	if (q_id < 0) { perror("msgget"); exit(-1); }

	if (DEBUG) printf("Dating Server ready...\n");
	i = 1;
	while(1) {
		// wait for a request for service
		actual_size = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), 1, 0);
		if (actual_size == -1) { perror("msgrcv"); break; }
		printf("Actual size=%d; Message %d: [%s]\n", actual_size, i++, msg_in.buf);
		
		//get string pid from the message
		char str_pid[strlen(msg_in.buf)-3];
		sprintf(str_pid, "%.*s", strlen(msg_in.buf)-3, &msg_in.buf[3]);
		
		//strncat(str_pid, msg_in.buf+3,strlen(msg_in.buf));
		int pid = atoi(str_pid);
		
		// male
		if(msg_in.buf[0] == 'M')
		{		
			
			if(msgs.empty() || q_has_males) //no females available
			{
				if (DEBUG) printf("dserver queueing pid: %d\n",pid);
				msg_out.mtype = pid;
				
				// easiest thing to do is to just quit
				if(msgs.full())
				{
					printf("FATAL ERROR: queue overflow in dserver\n");
					printf("Please run enddserver, increase QUEUE_MSG_SIZE\n");
					printf("in queue_msg.hpp, recompile and try again\n");
					exit(-3);
				}
				msgs.push(msg_out);
				q_has_males = 1;
			}
			else // queue has females
			{
				// get the message for a female
				msg_out = msgs.pop();
				msg_out.mtype = pid;
				
				// send the message to the male
				ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
				if (ret < 0) { perror("msgsnd"); exit(-2); }
				if (DEBUG) printf("dserver successfully sent a message to %d\n",
									msg_out.mtype);
			}
		}// end if male
		// female
		
		else if(msg_in.buf[0] == 'F')
		{
			if(msgs.empty() || (!q_has_males)) //no males available
			{
				if (DEBUG) printf("queueing pid: %d\n",pid);
				sprintf(msg_out.buf, "%d", pid);
				msg_out.mtype = 1;
				msgs.push(msg_out);
				q_has_males = 0;
			}
			else // queue has males
			{
				msg_out = msgs.pop();
				sprintf(msg_out.buf, "%d", pid);
				
				// send the message to the male
				ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
				if (ret < 0) { perror("msgsnd"); exit(-2); }
				if (DEBUG) printf("dserver successfully sent a message to %d\n",
									msg_out.mtype);
			}
		}// end if female
	}// end while loop

	if (DEBUG) printf("Dating Server shutting down...\n");
}


