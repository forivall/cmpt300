
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>
#include "global.h"

//================
// do not modify
//================
main () {
  if (DEBUG) printf("Attempting to shut down Dating Server...\n");
  int q_id = msgget(DSERV_QKEY, 0);
  if (q_id < 0) { perror("msgget"); exit(-1); }
  if (msgctl(q_id, IPC_RMID, NULL)) { perror("msgctl"); exit(-2); }
  if (DEBUG) printf("Dating Server queue terminated...\n");
}

