
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>
#include "global.h"

#define sex "female"


//==============
// write this!
//==============
main () {
	int mypid = getpid();
	int mate_pid;
	int q_id;
	int i;
	int ret;
	msg_60 msg_out, msg_in;

	if (DEBUG) printf("New %s, with pid = %d\n", sex, mypid);

	// find the message queue
	q_id = msgget(DSERV_QKEY, 0);
	if (q_id < 0) { perror("msgget"); exit(-1); }

	// send a message to the dating server, declaring availability
	sprintf(msg_out.buf, "F: %d", mypid);
	msg_out.mtype = 1;
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully sent info to dserver.\n", mypid);
	
	//wait for a message from a male
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully recieved info: %s.\n", mypid, msg_in.buf);
	
	mate_pid = atoi(msg_in.buf);
	
	// send confirmation to male
	msg_out.mtype = mate_pid;
	sprintf(msg_out.buf, "Yes!");
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully sent confirmation to %d.\n", mypid, mate_pid);
	
	//send a few messages to mate
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	if (DEBUG) printf("pid: %d recieved message:%s\n", mypid, msg_in.buf);
	
	msg_out.mtype = mate_pid;
	sprintf(msg_out.buf, "Hey hot stuff.", mypid);
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	if (DEBUG) printf("pid: %d recieved message: %s\n", mypid, msg_in.buf);
	
	if (DEBUG) printf("pid: %d shutting down.\n", mypid);
}
