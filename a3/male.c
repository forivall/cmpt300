
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/msg.h>
#include "global.h"

#define sex "male"


//==============
// write this!
//==============
main () {
	int mypid = getpid();
	int mate_pid;
	int q_id;
	int i;
	int ret;
	msg_60 msg_out, msg_in;

	if (DEBUG) printf("New %s, with pid = %d\n", sex, mypid);

	// find the message queue
	q_id = msgget(DSERV_QKEY, 0);
	if (q_id < 0) { perror("msgget"); exit(-1); }	
	
	// send a message to the dating server, declaring availability
	sprintf(msg_out.buf, "M: %d", mypid);
	msg_out.mtype = 1;
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully sent info to dserver.\n", mypid);
	
	// wait for a message from the dating server
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully recieved info from dserver.\n", mypid);
	
	// get pid of female from message
	mate_pid = atoi(msg_in.buf);
	
	// send message to female
	msg_out.mtype = mate_pid;
	sprintf(msg_out.buf, "%d", mypid);
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	if (DEBUG) printf("pid: %d successfully sent a request to a female.\n",mypid);
	
	// wait for confirmation from female
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	
	if (DEBUG) printf("pid: %d recieved confirmation from %d\n", mypid, mate_pid);

	
	//send a few messages to mate
	msg_out.mtype = mate_pid;
	sprintf(msg_out.buf, "Hey honey", mypid);
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	
	ret = msgrcv(q_id, &msg_in, sizeof(msg_60) - sizeof(long int), mypid, 0);
	if (ret == -1) { perror("msgrcv"); exit(-2); }
	if (DEBUG) printf("pid: %d recieved message: %s\n", mypid, msg_in.buf);
	
	msg_out.mtype = mate_pid;
	sprintf(msg_out.buf, "Let's blow this popsicle stand.", mypid);
	
	ret = msgsnd(q_id, &msg_out, sizeof(msg_60) - sizeof(long int), IPC_NOWAIT);
	if (ret < 0) { perror("msgsnd"); exit(-2); }
	
	if (DEBUG) printf("pid: %d shutting down.\n", mypid);
}

