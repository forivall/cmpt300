// 64 byte fixed length long long int queue class
// Jordan Klassen, Feb 2009

// commented code is for debugging
// methods are self explanatory

//#include <iostream>

#include "global.h"

#include "queue_msg.hpp"


queue_msg::queue_msg():
	back(0),
	front(0),
	size(0)
{
}

queue_msg::~queue_msg()
{
}

bool queue_msg::empty(){
	return (size <= 0);
}
bool queue_msg::full(){
	return (size >= QUEUE_MSG_SIZE);
}

int queue_msg::get_size(){
	return size*QUEUE_MSG_SIZE;
}

msg_60 queue_msg::pop(){
	int ind = front;
	front = (front+1)%QUEUE_MSG_SIZE;
	--size;
	return data[ind];
	
}
void queue_msg::push(msg_60 in){
	data[back] = in;
	back = (back+1)%QUEUE_MSG_SIZE;
	++size;
}

/* void queue_msg::print()
{
	int j = front;
	for(int i = 0;i<size;++i)
	{
		long long out = data[j];
		std::cout << out << ",";
		j = (j+1)%10;
	}
} */
