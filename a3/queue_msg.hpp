// 64 byte fixed length long long int queue class
// Jordan Klassen, Feb 2009

#ifndef _queue_msg_h_
#define _queue_msg_h_

#include "global.h"

#define QUEUE_MSG_SIZE 64

class queue_msg
{
public:
	queue_msg();
	~queue_msg();
	bool empty();
	bool full();
	int get_size();
	msg_60 pop();
	void push(msg_60);
	/* void print(); */
	
private:
	msg_60 data[QUEUE_MSG_SIZE];
	int back;
	int front;
	int size;
};


#endif /* queue64_hpp */
