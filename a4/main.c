/**
 * Main for filecount program
 * Jordan Klassen, March 2009
 * 
 */
 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>

// will spam info about the files being processed
#define DEBUG 0

// will show the current number of files processed
// doesn't spam
#define SHOW_STATUS 1

int numfiles = 0;

int numfragmented[64];

int numblocks[4];

void filecount (DIR*);
void storefrag (int);
void output ();

main (int argc, char **argv) {
	
	char *dirname = ".";
	DIR *dirptr;
	
	if(argc > 1) {
		if(argc > 2) {
			printf("Non-fatal error: more than one argument\n");
		}
		dirname= argv[1];
	}
	
	dirptr = opendir(dirname);
	
	if(dirptr == NULL) {
		printf("Fatal error: cannot open directory %s\n", dirname);
		return -1;
	}
	chdir(dirname);
	
	filecount(dirptr);

	closedir(dirptr);
	
	if(SHOW_STATUS) printf("\r                    ");
	
	printf("\nFor directory %s\n\n", dirname);
	output();

} /* main */

// recursive function that traverses through the directories, etc.
void filecount (DIR *dirptr) {
	struct stat buf; 
	
	dirent *entry = readdir(dirptr);
	
	/*
	The following POSIX macros are defined to check the file type using the
       st_mode field:
              S_ISREG(m)  is it a regular file?
              S_ISDIR(m)  directory?
              S_ISCHR(m)  character device?
              S_ISBLK(m)  block device?
              S_ISFIFO(m) FIFO (named pipe)?
              S_ISLNK(m)  symbolic link? (Not in POSIX.1-1996.)
              S_ISSOCK(m) socket? (Not in POSIX.1-1996.)
	*/
	
	for(int i=0;entry != NULL;++i)
	{
		lstat(entry->d_name, &buf);
		
		// File ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if(S_ISREG(buf.st_mode)) {
			
			int bytes = buf.st_size;
			//if(DEBUG) printf("%d\t", buf.st_size);
			if(DEBUG) printf("%s\n", entry->d_name);
			
			storefrag(buf.st_size);
			++numfiles;
			if(SHOW_STATUS) printf("\r%d", numfiles);
		}
		// Directory ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//else if((S_ISDIR(buf.st_mode)) && (i > 1)) {
		// the i > 1 is to ignore . and ..
		  // DOESN'T WORK ON MY CURRENT COMPUTER (2012-07)
		
		else if((S_ISDIR(buf.st_mode)) && 
		    (strcmp(entry->d_name,".") != 0) && 
		    (strcmp(entry->d_name,"..") != 0)) {
			
			//if(DEBUG) printf("%d\t", buf.st_size);
			if(DEBUG) { printf("dir:   %s\n", entry->d_name); }
			
			// recurse into directory
			DIR *newdir = opendir(entry->d_name);
			if (newdir != NULL)
			{
				chdir(entry->d_name);
				//filecount(newdir);
				chdir("..");
				closedir(newdir);
			}
			//if(DEBUG) { printf("exit:  %s\n", entry->d_name); }
		}
		
		// for next iteration
		entry = readdir(dirptr);
	}
}

// calculates the number of blocks and amount of internal fragmentation
void storefrag (int size) {
	// calculates which bucket it should go into
	// tested on paper, even works with files of size 0
	//++numfragmented[(63-(size-1)/64)%64];
	if(size == 0) {
		numfragmented[63] += 1;}
	else {
		numfragmented[63-((size-1)/64)%64] += 1;}
		
	int numblocks_minone = size/4096;
	
	if (numblocks_minone < 1) {
		++numblocks[0];
	}
	else if (numblocks_minone < 2) {
		++numblocks[1];
	}
	else if (numblocks_minone < 4) {
		++numblocks[2];
	}
	else if (numblocks_minone < 10){
		++numblocks[3];
	}
}

// outputs, duh
// also converts to percentage
void output () {
	int tempbuckets[64];
	
	for(int i=0;i<64;++i)
		tempbuckets[i] = (int)((float)(numfragmented[i]*100) / numfiles +0.5);
		
		
	for(int i=30;i>=1;--i) {
		if(i%5 == 0)
			printf(" %2d%% |", i);
		else
			printf("     |");
		
		for(int j=0;j<64;++j) {
			if(tempbuckets[j] >= i)
				printf("*");
			else printf(" ");
		}
		
		if(i%5 == 0)
			printf("| %2d%%\n", i);
		else
			printf("|\n");
	}
	
	printf("     +---'`--'`--'`--'`--'`--'`--'`--'`--'`--'`--'`--'`--'`--'`--'`---+\n");
	printf("     0               1K              2K              3K              4K\n");
	printf("                           Internal Fragmentation\n\n\n");
	
	int tempnums[4];
	
	for(int i=0;i<4;++i)
		tempnums[i] = (int)((float)(numblocks[i]*100) / numfiles +0.5);
	
	printf("Percentage of files:  1 block or less = %d%%\n", tempnums[0]);
	printf("                     2 blocks or less = %d%%\n", tempnums[1]);
	printf("                     4 blocks or less = %d%%\n", tempnums[2]);
	printf("                    10 blocks or less = %d%%\n", tempnums[3]);
}
