/* 
 * Main for cal program
 * Jordan Klassen, 2008
 * 
 */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void printLineFrom(int);
void printNoSpaceLineFrom(int);


main (int argc, char **argv) {
	
	//char bin[32];
	char command[32];
	int month, year, ret;
	
	int fd;
	
	time_t timevar = time(NULL);
	time_t *timevar_p = &timevar;
	
	// get real location of cal ~~~~~~~~~~~~~~~~~~~~~~~~~~~
	char cmd_whereis[24];
	
	int fd_whereis[2];
	if(pipe(fd_whereis) == -1) {
		printf("error creating pipe");
		return -1;
	}
	fd = fd_whereis[1];
	sprintf(cmd_whereis, "whereis -b cal >&%d", fd);
	
	system(cmd_whereis);
	
	
	char bin_temp[32];
	ret = read(fd_whereis[0], bin_temp, 32);
	bin_temp[ret] = '\0';
	
	close(fd_whereis[0]);
	
	// char bin[strlen(bin_temp)-5];
	// sprintf(bin, "%s", &bin_temp[5]);
	
	char *bin = strchr(bin_temp, ' ')+1;
	char *secondspace = strchr(bin, ' ');
	if(secondspace != NULL) {
		*secondspace = '\0';
	}
	else {
	secondspace = strchr(bin, '\n');
	*secondspace = '\0';
	}
	
	//bin[strlen(bin)-1] = '\0';
	
	//printf("%s\n", bin);
	//sprintf(bin, "cal");
	
	
	// parse args ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	switch (argc) {
	case 1:
		struct tm *curtime;
		curtime = localtime(timevar_p);
		month = curtime->tm_mon + 1;
		year = curtime->tm_year + 1900;
		
		break;
	case 2: 
		sprintf(command, "%s %s", bin, argv[1]);
		system(command);
		return 0;
	case 3: 
		month = atoi(argv[1]);
		year = atoi(argv[2]);
		if(month == 0 || year == 0) {
			sprintf(command, "%s %s %s", bin, argv[1], argv[2]);
			system(command);
			return -1;
		}
		break;
	default: 
		printf("usage: cal [ [month] year ]\n");
		return -1;
	} // end arg parsing ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	char bool_pre = 1, bool_post = 1;
	char command_pre[32];
	char command_post[32];
	
	// create pipes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	int fd_pre[2], fd_now[2], fd_post[2];
	ret = pipe(fd_pre);
	if(ret == -1) {
		printf("error creating pipe");
		return -1;
	}
	ret = pipe(fd_now);
	if(ret == -1) {
		printf("error creating pipe");
		return -1;
	}
	ret = pipe(fd_post);
	if(ret == -1) {
		printf("error creating pipe");
		return -1;
	}
	//printf("%d\n", ret);
	
	// create commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if(month == 1) {
		if(year > 1)
			sprintf(command_pre, "%s %d %d >&%d", 
				bin, 12, year-1, fd_pre[1]);
		else bool_pre = 0;
	}
	else {
		sprintf(command_pre, "%s %d %d >&%d", 
			bin, month-1, year, fd_pre[1]);
	}
	
	sprintf(command, "%s %d %d >&%d", 
		bin, month, year, fd_now[1]);
	
	if(month == 12) {
		if(year < 9999)
			sprintf(command_post, "%s %d %d >&%d", 
				bin, 1, year+1, fd_post[1]);
		else bool_post = 0;
	}
	else {
		sprintf(command_post, "%s %d %d >&%d", 
			bin, month+1, year, fd_post[1]);
	}
	//printf("%s\n",command_pre);
	if(bool_pre) system(command_pre);
	close(fd_pre[1]);
	
	//printf("%s\n",command);
	system(command);
	close(fd_now[1]);
	
	//printf("%s\n",command_post);
	
	// this empty printf fixes the most retarded bug ever
	printf("");
	
	
	if(bool_post) system(command_post);
	close(fd_post[1]);
	
	close(fd_whereis[1]);
	
	for(int i=0; i<7;++i){
		if(bool_pre) printLineFrom(fd_pre[0]);
		if(bool_post) printLineFrom(fd_now[0]);
		else printNoSpaceLineFrom(fd_now[0]);
		if(bool_post) printNoSpaceLineFrom(fd_post[0]);
		printf("\n");
	}
	
	// printAll(fd_pre[0]);
	// printAll(fd_now[0]);
	// printAll(fd_post[0]);
	
	printf("\n");
	
	
	
	close(fd_pre[0]);
	
	close(fd_now[0]);
	
	close(fd_post[0]);
	
}

void printLineFrom(int fd) {
	char buf[1];
	char eol = 0;
	for(int i=0;i<24;++i) {
			if(eol) printf(" ");
			else {
				int ret = read(fd, buf, 1);
				if((buf[0] == '\n') || (ret == 0))
					eol = 1;
				else printf("%c", buf[0]);
			}
		}
}

void printNoSpaceLineFrom(int fd) {
	char buf[1];
	char eol = 0;
	for(int i=0;i<24;++i) {
			int ret = read(fd, buf, 1);
			if((buf[0] == '\n') || (ret == 0))
				break;
			else printf("%c", buf[0]);
		}
}

