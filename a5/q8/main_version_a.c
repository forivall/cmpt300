
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

main (int argc, char **argv) {
	
	char command[32];
	int month, year;
	time_t timevar = time(NULL);
	time_t *timevar_p = &timevar;
	
	switch (argc) {
	case 1:
		struct tm *curtime;
		curtime = localtime(timevar_p);
		month = curtime->tm_mon + 1;
		year = curtime->tm_year + 1900;
		
		break;
	case 2: 
		sprintf(command, "cal %s", argv[1]);
		system(command);
		return 0;
	case 3: 
		month = atoi(argv[1]);
		year = atoi(argv[2]);
		if(month == 0 || year == 0) {
			sprintf(command, "cal %s %s", argv[1], argv[2]);
			system(command);
			return -1;
		}
		break;
	default: 
		printf("usage: cal [ [month] year ]\n");
		return -1;
	}
	char command_pre[32];
	char command_post[32];
	
	if(month == 1) {
		if(year > 1)
			sprintf(command_pre, "cal %d %d", 12, year-1);
		else sprintf(command_pre, "");
	}
	else {
		sprintf(command_pre, "cal %d %d", month-1, year);
	}
	
	sprintf(command, "cal %d %d", month, year);
	
	if(month == 12) {
		if(year < 9999)
			sprintf(command_post, "cal %d %d", 1, year+1);
		else sprintf(command_post, "");
	}
	else {
		sprintf(command_post, "cal %d %d", month+1, year);
	}
	
	// TODO: fancy piping stuff
	
	system(command_pre);
	system(command);
	system(command_post);
	// system("echo Hello World");
	// system("cal");
	// system("cal 4 2009");
	// system("cal 2009");
}

